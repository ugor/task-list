import { Component, OnInit} from '@angular/core';
import { TaskServices } from '../services/task.services';
import { Task } from '../model/task';

@Component({
  selector: 'app-done-task',
  templateUrl: './done-task.component.html',
  styleUrls: ['./done-task.component.css']
})
export class DoneTaskComponent implements OnInit {

  doneList: Array<Task> = [];

  constructor(private taskService: TaskServices) {
    this.taskService.getDoneListObservable().subscribe((tasks: Array<Task>) => {
      this.doneList = tasks;
    });
  }

  ngOnInit(): void {
  }

}
