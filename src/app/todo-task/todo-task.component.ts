import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TaskServices } from '../services/task.services';
import { Task } from '../model/task';

@Component({
  selector: 'app-todo-task',
  templateUrl: './todo-task.component.html',
  styleUrls: ['./todo-task.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TodoTaskComponent implements OnInit {

  taskList: Array<Task> = [];

  constructor(private taskService: TaskServices) {
    this.taskService.getTaskListObservable().subscribe((tasks: Array<Task>) => {
      this.taskList = tasks.slice();
    });
  }

  ngOnInit(): void {
  }

  done(task: Task) {
    task.end = new Date();
    this.taskService.done(task);
  }

  remove(task: Task) {
    this.taskService.remove(task);
  }

  getColor(): string {
    return this.taskList.length >= 5 ? 'red' : 'green';
  }

}
