import { Component, OnInit } from '@angular/core';
import { TaskServices } from '../services/task.services';
import { Task } from '../model/task';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent implements OnInit {

  newTask: string;

  constructor(private taskService: TaskServices) { }

  ngOnInit(): void {
  }

  add(){
    const task: Task = ({ name: this.newTask, created: new Date()});
    this.taskService.add(task);
    this.newTask = '';
  }

}
