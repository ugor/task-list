import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Task } from '../model/task';

@Injectable()
export class TaskServices {

  private taskList: Array<Task> = [];
  private doneList: Array<Task> = [];

  private taskListObservable = new BehaviorSubject<Array<Task>>([]);
  private doneListObservable = new BehaviorSubject<Array<Task>>([]);

  constructor() {
    this.taskList = [
      { name: 'todo3', created: new Date() },
      { name: 'todo4', created: new Date() },
      { name: 'todo1', created: new Date() },
      { name: 'todo2', created: new Date() }
    ];
    this.taskListObservable.next(this.taskList);
  }

  add(task: Task) {
    this.taskList.push(task);
    this.taskListObservable.next(this.taskList);
  }

  remove(task: Task) {
    this.taskList = this.taskList.filter(e => e !== task);
    this.taskListObservable.next(this.taskList);
  }

  done(task: Task) {
    this.doneList.push(task);
    this.remove(task);
    this.doneListObservable.next(this.doneList);
  }

  getTaskListObservable(): Observable<Array<Task>> {
    return this.taskListObservable.asObservable();
  }

  getDoneListObservable(): Observable<Array<Task>> {
    return this.doneListObservable.asObservable();
  }
}
